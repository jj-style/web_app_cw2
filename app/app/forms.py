from flask_wtf import FlaskForm as Form
from wtforms import IntegerField, StringField, PasswordField, TextAreaField
from wtforms.validators import DataRequired, Length, EqualTo, ValidationError
from app import models

class Signup(Form):
    user_name = StringField("User Name", validators=[DataRequired(), Length(6,16, "Username must be between %(min)d and %(max)d characters")])
    password = PasswordField("Password", validators=[DataRequired(),
                                                    Length(8,20, "Password must be between %(min)d and %(max)d characters")])
    confirm = PasswordField("Confirm Password", validators=[DataRequired(),
                                                EqualTo('password', message='Passwords must match')])
    def validate_password(form, field):
        pwd = field.data
        upper = 1
        lower = 1
        digit = 1
        for char in pwd:
            if char.isalpha() and char.islower():
                lower -= 1
            elif char.isalpha() and char.isupper():
                upper -= 1
            elif char.isdigit():
                digit -= 1
        valid = not (lower or upper or digit)
        valid = False
        if (lower < 1 and upper < 1 and digit < 1):
            valid = True
        if not valid:
            raise ValidationError(message = 'Password must contain at least:\
            {} uppercase letter, {} lowercase letter and {} digit'.format(1,1,1))

    def validate_user_name(form, field):
        if len(models.User.query.filter_by(user_name=field.data).all()) == 1:
            raise ValidationError(message = 'Username already exists.')

class Login(Form):
    user_name = StringField("User Name", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])

class PostForm(Form):
    title = StringField("Title", validators=[DataRequired(), Length(1,128)])
    content = TextAreaField("Content", validators=[DataRequired()])
    tags = StringField("Tags")
    
class CommentForm(Form):
    content = TextAreaField("Comment Content", validators=[DataRequired()])

class ChangePassword(Form):
    current = PasswordField("Current Password", validators=[DataRequired()])
    new = PasswordField("New Password", validators=[DataRequired(),
                                                    Length(8,20, "Password must be between %(min)d and %(max)d characters")])
    confirm_new = PasswordField("Confirm New Password", validators=[DataRequired(),
                                                EqualTo('new', message='Passwords must match')])
    def validate_password(form, field):
        pwd = field.data
        upper = 1
        lower = 1
        digit = 1
        for char in pwd:
            if char.isalpha() and char.islower():
                lower -= 1
            elif char.isalpha() and char.isupper():
                upper -= 1
            elif char.isdigit():
                digit -= 1
        valid = not (lower or upper or digit)
        valid = False
        if (lower < 1 and upper < 1 and digit < 1):
            valid = True
        if not valid:
            raise ValidationError(message = 'Password must contain at least:\
        \n{} uppercase letter\n{} lowercase letter\n{} digit'.format(1,1,1))