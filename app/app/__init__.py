from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_misaka import Misaka
from flask_login import login_manager
from flask_bcrypt import Bcrypt
import logging

app = Flask(__name__)
app.config.from_object('config')
Misaka(app, escape=True, fenced_code=True, math=True, math_explicit=True, strikethrough=True,
            superscript=True, tables=True, footnotes=True, autolink=True, smartypants=True)
bcrypt = Bcrypt(app)
db = SQLAlchemy(app)

login_manager = login_manager.LoginManager()
login_manager.init_app(app)

migrate = Migrate(app, db)

logging.basicConfig(filename='app.log',level=logging.DEBUG, format = '[%(asctime)s] %(levelname)s - %(message)s')

from app import views, models