from flask import render_template, flash, redirect, url_for, request, session, abort
from sqlalchemy import and_
from app import app, models, db, bcrypt
from .forms import Signup, Login, PostForm, CommentForm, ChangePassword
import json, re
from app import login_manager
from flask_login import login_required, login_user, logout_user, current_user
from sqlalchemy import and_
from datetime import datetime

MAX_POSTS_PER_PAGE = 3
APP_NAME = "Segmentation fault"

@app.route('/index', methods=['GET','POST'])
@app.route('/', methods=['GET','POST'])
def index():
    # homepage of web app displaying login form. Redirects already logged in users to /feed
    # receives login form submission and attempts to log the user in
    try: # try necessary as not-logged in users are anonymous objects with no .is_authenticated()
        if current_user.is_authenticated():
            return redirect(url_for('feed'))
    except:
        pass
    form = Login()
    errors = []
    if form.validate_on_submit():
        try:
            # attempt to find user with given username
            user = models.User.query.filter(models.User.user_name==form.user_name.data).first()
            if user: # user found
                if bcrypt.check_password_hash(user.password, form.password.data):
                    # if hashed passwords match login with flask-login and redirect to /feed
                    user.authenticated = True
                    db.session.commit()
                    login_user(user, remember=True)
                    app.logger.info('LOGIN {} OK'.format(user.user_name))
                    return redirect(url_for('feed'))
                else:
                    app.logger.error('LOGIN {} ERROR - INCORRECT PASSWORD'.format(form.user_name.data))
                    errors.append("Username or password incorrect. Please try again")
            else:
                app.logger.warning('LOGIN {} ERROR - USER NOT FOUND'.format(form.user_name.data))
                errors.append("Username or password incorrect. Please try again")
        except Exception as e:
            # log any errors that occur and redirect to homepage
            app.logger.error(e.args[0])
    return render_template("index_out.html", form=form, which_form=1,
                                    title=APP_NAME, app_name=APP_NAME, errors=errors)

@app.route('/dropdownFilter', methods=['POST'])
@login_required
def dropdownFilter():
    # receives AJAX request and returns a list of post names which match the search
    posts = models.Post.query.all()
    data = json.loads(request.data.decode('utf-8'))
    search = data.get("search")
    if len(search) > 0:
        posts = [x.title for x in posts if re.match(".*{}.*".format(re.escape(search)),x.title,re.I)]
    else: posts = []
    return json.dumps({'status': 'OK', 'response': posts})

@app.route('/searchPostTitle', methods=['POST'])
@login_required
def searchPostTitle():
    # searches for posts for a given post title
    # if exactly one posts matches - show that post's page
    # if more than one show them all in the feed as filtered results
    if request.method == 'POST':
        search = request.form.get('navsearch')
        posts = models.Post.query.all()
        posts = [x for x in posts if re.match(".*{}.*".format(re.escape(search)),x.title,re.I)]
        if len(posts) == 1:
            return redirect(url_for('post',num=posts[0].post_id))
        else:
            tags = models.Tag.query.order_by(models.Tag.count.desc()).all()
            post_form = PostForm()
            tag_filter = "all"
            return render_template("index_in.html", title="Feed | {}".format(APP_NAME), form=post_form,
                                    posts=posts, user=current_user, page_n=1,
                                    max_posts=MAX_POSTS_PER_PAGE, all_tags=tags,
                                    tag_filter=tag_filter, app_name=APP_NAME)
    return redirect(request.referrer)

@app.route('/tagSuggestion', methods=['POST'])
@login_required
def tagSuggestion():
    # receives AJAX request and returns a list of tag names which match the search
    tags = models.Tag.query.all()
    data = json.loads(request.data.decode('utf-8'))
    search = data.get("search")
    search = search.split(";")[-1]
    if len(search) > 0:
        tags = [x.tag_name for x in tags if re.match(".*{}.*".format(re.escape(search)),x.tag_name,re.I)]
    else: tags = []
    return json.dumps({'status': 'OK', 'response': tags})

@app.route('/feed', methods=['GET','POST'])
@login_required
def feed():
    # logged in users feed of posts with filtering allowed
    post_form = PostForm()
    tags = models.Tag.query.order_by(models.Tag.count.desc()).all()
    page = request.args.get('page')      # get which page of pagination to show
    tag_filter = request.args.get('tag') # get which tag to filter posts by
    if page == None: page = 1            # if page isn't specified default to page 1
    if tag_filter != None and tag_filter != "all":
        try: # try filtering posts with which contain the tag to filter by
            target_tag = models.Tag.query.filter(models.Tag.tag_name==tag_filter).first()
            posts = models.Post.query.filter(models.Post.tags.contains(target_tag)).order_by(models.Post.posted.desc()).all()
        except: posts = models.Post.query.order_by(models.Post.posted.desc()).all()
    else:
        posts = models.Post.query.order_by(models.Post.posted.desc()).all()
        tag_filter = "all"
    app.logger.info("FEED FILTER:{} TAG:{}".format(tag_filter, page))
    return render_template("index_in.html", title="Feed | {}".format(APP_NAME), form=post_form,
                            posts=posts, user=current_user, page_n=int(page),
                            max_posts=MAX_POSTS_PER_PAGE, all_tags=tags,
                            tag_filter=tag_filter, app_name=APP_NAME)

@app.route('/signup', methods=['GET','POST'])
def signup():
    # signup page displaying login form. Redirects already logged in users to /feed
    try:
        if current_user.is_authenticated():
            return redirect(url_for('feed'))
    except:
        pass
    signup_form = Signup()
    if signup_form.validate_on_submit():
        create_user(signup_form.user_name.data, signup_form.password.data)
        return redirect(url_for('index'))
    else:
        app.logger.warning("SIGNUP ERROR: Username:{} Password:{} Confirm-Password:{}".format(
            signup_form.user_name.errors, signup_form.password.errors, signup_form.confirm.errors))
    return render_template("index_out.html", form=signup_form, which_form=0,
                                title=APP_NAME, app_name=APP_NAME)

@app.route('/postForm', methods=['POST'])
@login_required
def postForm():
    # handles submissions of new posts and creates or updates tags if necessary
    log_msg = ""
    form = PostForm(request.form) # get the form the user submitted
    if form.validate_on_submit():
        post = models.Post(form.title.data, form.content.data, current_user.user_id, datetime.now())
        tags = (form.tags.data).split(";")
        tags = [x for x in tags if x]
        tags = set(tags)    # remove duplicate tags in post
        for tag in tags:
            format_tag = re.sub('[^A-Za-z" "]+', '', tag).lower()
            format_tag = " ".join(x for x in format_tag.split(" ") if x)
            if len(models.Tag.query.filter(models.Tag.tag_name==format_tag).all()) == 0:
                # if no tags with this tag name exist create a new tag
                new_tag = models.Tag(format_tag)
                db.session.add(new_tag)
                log_msg += "NEW TAG: {}\n".format(format_tag)
            t = models.Tag.query.filter(models.Tag.tag_name==format_tag).first()
            post.tags.append(t)
            t.count += 1
        db.session.add(post)
        log_msg += "NEW POST: {} by {}\n".format(form.title.data, current_user.user_name)
        db.session.commit()
        app.logger.info(log_msg)
    return redirect(request.referrer)

@app.route('/post/', methods=['GET','POST'])
@app.route('/post/<int:num>', methods=['GET','POST'])
@login_required
def post(num):
    # shows individual post's page and handles comment form submissions
    # redirects users to feed if post number not found
    post = models.Post.query.get(num)
    if post != None:
        app.logger.info("VIEW POST {}".format(num))
        comment_form = CommentForm()
        if comment_form.validate_on_submit():
            new_comment = models.Comment(current_user.user_id, num, comment_form.content.data, datetime.now())
            db.session.add(new_comment)
            db.session.commit()
            app.logger.info("NEW COMMENT, POST:{}, USER:{}".format(num, current_user.user_name))
        return render_template("post.html", post=post, comment_form=comment_form, app_name=APP_NAME)
    else:
        app.logger.info("POST {} NOT FOUND".format(num))
        return redirect(url_for('feed'))

@app.route('/deletePost/')
@login_required
def deletePost():
    # deletes a given post from the database
    # updates or deletes tags if necessary
    try:
        post = models.Post.query.get(request.args['post_id'])
        if post.author != current_user.user_id:
            app.logger.error("ERROR DELETE POST, USER:{} NOT AUTHOR OF POST:{}".format(current_user.user_id, post.post_id))
            return redirect(url_for('feed'))
        tag_msg = ""
        for tag in post.tags:
            stored_tag = models.Tag.query.get(tag.tag_id)
            stored_tag.count -= 1
            if stored_tag.count == 0:
                tag_msg += "DELETED TAG:{}".format(tag.tag_name)
                db.session.delete(tag)
        like_msg = ""
        for user in post.liked_by:
            like_msg += "DELETED USER:{} LIKE ON POST:{}".format(user.user_id, post.post_id)
            post.liked_by.remove(user)
        save_msg = ""
        for user in post.saved_by:
            save_msg += "DELETED USER:{} SAVE ON POST:{}".format(user.user_id, post.post_id)
            post.saved_by.remove(user)
        cmt_msg = ""
        for comment in post.comments:
            cmt_msg += "DELETED COMMENT ON POST:{}, COMMENT ID:{}, by USER:{}".format(post.post_id, comment.cmt_id, comment.user_id)
            post.comments.remove(comment)
        pst_msg = "DELETED POST {}:{}".format(post.post_id, post.title)
        db.session.delete(post)
        db.session.commit()
        app.logger.info(tag_msg)
        app.logger.info(like_msg)
        app.logger.info(save_msg)
        app.logger.info(cmt_msg)
        app.logger.info(pst_msg)
        return redirect(request.referrer)
    except Exception as e:
        app.logger.error("ERROR DELETE POST, SOMETHING WENT WRONG")
        app.logger.error(e.args[0])
        return redirect(request.referrer)

@app.route('/vote', methods=['POST'])
@login_required
def vote():
    # receives AJAX request to like or dislike a post
    data = json.loads(request.data.decode('utf-8'))
    vote_type = data.get("vote_type").split("-")
    up_or_down = vote_type[0]
    d={"up":True, "down":False}
    p_id = vote_type[1]
    # find out if user has already liked/disliked the post before
    already_liked = models.Likes.query.filter(
        and_(models.Likes.user_id == current_user.user_id, models.Likes.post_id==p_id)).all()
    post = models.Post.query.get(p_id)
    app.logger.info("NEW VOTE CAST ON POST:{} TYPE:{}".format(post.post_id, up_or_down))
    # if user hasn't liked or disliked the post before
    if len(already_liked) == 0:
        new_like = models.Likes(current_user.user_id, p_id, d[up_or_down])
        # create new like and increment or decrement the number of likes
        db.session.add(new_like)
        app.logger.info("CREATE NEW VOTE ON POST:{}".format(post.post_id))
        if d[up_or_down]:
            post.num_likes += 1
        else:
            post.num_likes -= 1
    else:
        app.logger.info("UPDATE EXISTING VOTE ON POST:{}".format(post.post_id))
        # if liked and like again remove the like
        # if liked and dislike, toggle like type and decrement likes by 2
        # if disliked and like, toggle like type and increment likes by 2
        # if disliked and dislike again remove the like
        like = already_liked[0]
        if like.like_type == True and d[up_or_down] == True:
            db.session.delete(like)
            post.num_likes -= 1
        elif like.like_type == True and d[up_or_down] == False:
            like.like_type ^= True
            post.num_likes -= 2
        elif like.like_type == False and d[up_or_down] == True:
            like.like_type ^= True
            post.num_likes += 2
        elif like.like_type == False and d[up_or_down] == False:
            db.session.delete(like)
            post.num_likes += 1
    db.session.commit()
    return json.dumps({'status': 'OK', 'updated':not already_liked, 'total':post.num_likes})

def create_user(user_name, password):
    # create new user, hash password and add to the database
    password_hash = bcrypt.generate_password_hash(password).decode("utf-8")
    new_user = models.User(user_name, password_hash)
    db.session.add(new_user)
    db.session.commit()
    app.logger.info("SIGN UP {} OK".format(user_name))

@app.route('/account', methods=['GET'])
@login_required
def account():
    # view account page - just shows all the posts that the user has posted
    app.logger.info("VIEW ACCOUNT PAGE FOR USER:{}".format(current_user.user_name))
    post_form = PostForm()
    posts = models.Post.query.filter(models.Post.author == current_user.user_id).order_by(models.Post.posted.desc()).all()
    return render_template("account.html", title="{}'s Account".format(current_user.user_name), form=post_form,
                            posts=posts, user=current_user, app_name=APP_NAME)

@app.route('/save', methods=['POST'])
@login_required
def save():
    # save/unsaves a post to users favourites
    try:
        data = json.loads(request.data.decode('utf-8'))
        save = data.get("save").split("-")
        post = models.Post.query.get(save[1])
        if post in current_user.saved:
            current_user.saved.remove(post)
            app.logger.info("User:{} unsaved post:{}".format(current_user.user_id, post.post_id))
        else:
            current_user.saved.append(post)
            app.logger.info("User:{} saved post:{}".format(current_user.user_id, post.post_id))
        db.session.commit()
        return json.dumps({'status': 'OK'})
    except Exception as e:
        app.logger.error("ERROR SAVE POST, POST NOT FOUND")
        app.logger.error(e.args[0])
        return redirect(request.referrer)

@app.route('/saved', methods=['GET'])
@login_required
def saved():
    # view users saved posts
    app.logger.info("VIEW ACCOUNT PAGE FOR USER:{}".format(current_user.user_name))
    post_form = PostForm()
    posts = current_user.saved
    return render_template("account.html", title="{}'s Account".format(current_user.user_name), form=post_form,
                            posts=posts, user=current_user, app_name=APP_NAME)

@app.route('/changePassword', methods=['GET', 'POST'])
@login_required
def changePassword():
    # change password of the user
    change_pwd_form = ChangePassword()
    changed=False
    user = models.User.query.get(current_user.user_id)
    if change_pwd_form.validate_on_submit():
        if bcrypt.check_password_hash(user.password, change_pwd_form.current.data):
            new_pwd_hash = bcrypt.generate_password_hash(change_pwd_form.new.data).decode("utf-8")
            user.password = new_pwd_hash
            db.session.commit()
            app.logger.info("USER:{} CHANGED PASSWORD".format(current_user.user_name))
            changed=True
        else:
            app.logger.warning("WRONG CURRENT PASSWORD ENTERED FOR USER:{}".format(current_user.user_name))
    else:
        if change_pwd_form.is_submitted():
            if not bcrypt.check_password_hash(user.password, change_pwd_form.current.data): 
                change_pwd_form.current.errors.append("Current password incorrect")
            app.logger.warning("FAILED ATTEMPT FOR USER:{} TO CHANGE PASSWORD".format(current_user.user_name))
    return render_template("change_password.html", title="Change Password", form=change_pwd_form,
                            app_name=APP_NAME, changed=changed)

#flask-login stuff
@login_manager.user_loader
def load_user(user_id):
    return models.User.query.get(user_id)

@app.route("/logout")
@login_required
def logout():
    app.logger.info("LOGOUT {} OK".format(current_user.user_name))
    current_user.authenticated = False
    db.session.commit()
    logout_user()
    return redirect(url_for('index'))

@login_manager.unauthorized_handler
def unauthorized_callback():
    app.logger.info("ATTEMPT TO ACCESS FEED BEFORE LOGIN")
    return redirect(url_for('index'))