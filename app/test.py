import unittest
import os, random
from datetime import datetime
from app import app, views, models, db
from app.models import User, Post, Comment, Tag, Likes
__unittest = True

class Tests(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(os.path.abspath(os.path.dirname(__file__)),
        'test.db')
        db.drop_all()
        db.create_all()

        self.app = app.test_client()
        self.app.testing = True

        self.user = User("username", "Password123")
        self.user_duplicate_username = User("username", "DifferentPassword")
        self.tag = Tag("tag")
        self.tag_duplicate_tag_name = Tag("tag")

    def test_add_user(self):
        db.session.add(self.user)
        db.session.commit()
        self.assertEqual(len(User.query.all()), 1)
        self.assertEqual(User.query.first(), self.user)

    @unittest.expectedFailure
    def test_user_db_constraint(self):
        db.session.add(self.user)
        db.session.add(self.user_duplicate_username)
        db.session.commit()
        self.assertTrue(True)

    def test_add_post(self):
        db.session.add(self.user)
        db.session.commit()
        post = Post("Post Title", "Post content with some **markdown**", User.query.first().user_id, datetime.now())
        db.session.add(post)
        db.session.commit()
        self.assertEqual(len(Post.query.all()), 1)
        self.assertEqual(Post.query.first(), post)
        self.assertEqual(User.query.get(Post.query.first().author), self.user)

    def test_add_tag(self):
        db.session.add(self.tag)
        db.session.commit()
        self.assertEqual(len(Tag.query.all()), 1)
        self.assertEqual(Tag.query.first(), self.tag)

    @unittest.expectedFailure
    def test_tag_db_constraint(self):
        db.session.add(self.tag)
        db.session.add(self.tag_duplicate_tag_name)
        db.session.commit()
        self.assertTrue(True)

    def test_add_comment(self):
        db.session.add(self.user)
        post = Post("Post Title", "Post content with some **markdown**", User.query.first().user_id, datetime.now())
        db.session.add(post)
        db.session.commit()

        comment_text = "Comment on post"
        comment = Comment(User.query.first().user_id, Post.query.first().post_id, comment_text, datetime.now())
        db.session.add(comment)
        db.session.commit()
        self.assertEqual(len(Comment.query.all()), 1)
        self.assertEqual(User.query.get(Comment.query.first().user_id), self.user)
        self.assertEqual(Post.query.get(Comment.query.first().post_id), post)
        self.assertEqual(Comment.query.first().comment, comment_text)

    def test_add_like(self):
        db.session.add(self.user)
        post = Post("Post Title", "Post content with some **markdown**", User.query.first().user_id, datetime.now())
        db.session.add(post)
        db.session.commit()

        like = Likes(User.query.first().user_id, Post.query.first().post_id, True)
        db.session.add(like)
        db.session.commit()
        like_db = Likes.query.first()
        self.assertEqual(like_db.user_id, User.query.first().user_id)
        self.assertEqual(like_db.post_id, Post.query.first().post_id)
        self.assertEqual(User.query.first().likes[0], Post.query.first())
        self.assertEqual(Post.query.first().liked_by[0], User.query.first())

    def test_logged_out_pages(self):
        # GET request for each route logged out users can access
        routes = ['/', '/index' ,'/signup']
        for route in routes:
            result = self.app.get(route)
            self.assertRegex(str(result.status_code), "2\d\d")

    def test_non_existant_page(self):
        # 100 tests generating random route names and trying a GET request
        for i in range(100):
            route = "/"
            for i in range(5,random.randint(10,20)):
                route += chr(random.randint(97,122))
            result = self.app.get(route)
            self.assertRegex(str(result.status_code), "4\d\d")

    def test_feed_logged_in_get_pages(self):
        # GET request for each route logged in users can access
        routes = ['/feed', '/post' ,'/post/1', '/account', '/saved', '/changePassword']
        for route in routes:
            result = self.app.get(route)
            self.assertRegex(str(result.status_code), "3\d\d")

    def test_feed_logged_in_post_pages(self):
        # GET request for each route logged in users can access
        routes = ['/dropdownFilter', '/searchPostTitle' ,'/tagSuggestion', '/feed', '/postForm',
                    '/post', '/post/1', '/vote', '/save', '/changePassword']
        for route in routes:
            result = self.app.post(route)
            self.assertRegex(str(result.status_code), "3\d\d")

    def tearDown(self):
        db.session.remove()
        db.drop_all()

if __name__ == '__main__':
    unittest.main()
