from app import db
from datetime import datetime

class Likes(db.Model):
    __tablename__ = 'likes'
    user_id = db.Column(db.Integer, db.ForeignKey('users.user_id'), primary_key=True, nullable=False)
    post_id = db.Column(db.Integer, db.ForeignKey('post.post_id'), primary_key=True, nullable=False)
    
    like_type = db.Column(db.Boolean)

    user = db.relationship("User", backref=db.backref("user_association"))
    module = db.relationship("Post", backref=db.backref("post_association"))

    def __init__(self, u_id, p_id, l_type):
        self.user_id = u_id
        self.post_id = p_id
        self.like_type = l_type

class User(db.Model):
    __tablename__ = "users"
    user_id = db.Column(db.Integer, primary_key=True)
    user_name = db.Column(db.String(16), unique=True)
    password = db.Column(db.String(60))
    authenticated = db.Column(db.Boolean, default=False)

    likes = db.relationship("Post", secondary="likes", lazy=True)
    comments = db.relationship("Comment", lazy=True)
    saved = db.relationship("Post", secondary="saves", lazy=True)

    def is_active(self):
        return True

    def get_id(self):
        return self.user_id

    def is_authenticated(self):
        return self.authenticated

    def is_anonymous(self):
        return False
        
    def __repr__(self):
        return self.user_name

    def __init__(self, uname, pwd):
        self.user_name = uname
        self.password = pwd

tags = db.Table("tags",
    db.Column("tag_id", db.Integer, db.ForeignKey("tag.tag_id"), primary_key=True),
    db.Column("post_id", db.Integer, db.ForeignKey("post.post_id"), primary_key=True)
)

saves = db.Table("saves",
    db.Column("user_id", db.Integer, db.ForeignKey("users.user_id"), primary_key=True),
    db.Column("post_id", db.Integer, db.ForeignKey("post.post_id"), primary_key=True)
)

class Post(db.Model):
    __tablename__ = "post"
    post_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(128))
    content = db.Column(db.Text)
    posted = db.Column(db.DateTime, default=datetime.now())
    author = db.Column(db.Integer, db.ForeignKey("users.user_id"), nullable=False)
    num_likes = db.Column(db.Integer, default=0)

    tags = db.relationship("Tag", secondary=tags, lazy="subquery",
        backref=db.backref("pages", lazy=True))
    liked_by = db.relationship("User", secondary="likes", lazy=True)
    comments = db.relationship("Comment", lazy=True)
    saved_by = db.relationship("User", secondary="saves", lazy=True)

    user = db.relationship('User', foreign_keys='Post.author')

    def __init__(self, title, content, author, dt):
        self.title = title
        self.content = content
        self.author = author
        self.posted = dt

    def like_type_by(self, user):
        if user in self.liked_by:
            for like in self.post_association:
                if like.user_id == user.user_id:
                    return like.like_type
        return -1
        

class Tag(db.Model):
    __tablename__ = "tag"
    tag_id = db.Column(db.Integer, primary_key=True)
    tag_name = db.Column(db.String(16), unique=True)
    count = db.Column(db.Integer, default=0)

    def __init__(self, t_name):
        self.tag_name = t_name

class Comment(db.Model):
    __tablename__ = "comment"
    cmt_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.user_id'))
    post_id = db.Column(db.Integer, db.ForeignKey('post.post_id'))
    comment = db.Column(db.Text)
    date_time = db.Column(db.DateTime, default=datetime.now())

    user = db.relationship('User', foreign_keys='Comment.user_id')

    def __init__(self, u_id, p_id, com, dt):
        self.user_id = u_id
        self.post_id = p_id
        self.comment = com
        self.date_time = dt

    def __repr__(self):
        return self.comment
