$(document).ready(function() {

    $('#search-field').on('input', function() {
        var val = $("#search-field").val()
        $.ajax({
            type: "POST",
            url: "/dropdownFilter",
            data: JSON.stringify({
                search: val
            }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function(response) {
                var results = response.response;
                console.log(results);
                $("#search-field").autocomplete({
                    source: results
                });
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    $('#tags_in').on('input', function() {
        var val = $("#tags_in").val()
        $.ajax({
            type: "POST",
            url: "/tagSuggestion",
            data: JSON.stringify({
                search: val
            }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function(response) {
                var results = response.response;
                console.log(results);
                $("#tags_in").autocomplete({
                    source: results
                });
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    $("a.vote").on("click", function() {
        var clicked = $(this);
        console.log("voted " + $(clicked).attr("id"));
        $.ajax({
            type: "POST",
            url: "vote",
            data: JSON.stringify({
                vote_type: $(this).attr("id")
            }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function(response) {
                console.log("Updated: " + response.updated);
                $(clicked).parent().children()[2].innerHTML = response.total;
                var clicked_list = $(clicked).attr("id").split("-");
                if (clicked_list[0] === "up")
                    var other = "down-" + clicked_list[1];
                else
                    var other = "up-" + clicked_list[1];
                console.log("other: " + other);
                console.log(document.URL + " #" + $(clicked).attr("id"));
                console.log("#" + "likes-"+clicked_list[1]);
                $(clicked).load(document.URL + " #" + $(clicked).attr("id"));
                $("#" + other).load(document.URL + " #" + $("#" + other).attr("id"));
                $("#" + "likes-"+clicked_list[1]).load(document.URL + " #" + $("#" + "likes-"+clicked_list[1]).attr("id"));
            },
            error: function(error) {
                console.log("error");
            }
        });
    });

    $(".save").on("click", function() {
        var clicked = $(this);
        console.log("clicked " + $(clicked).attr("id"));
        $.ajax({
            type: "POST",
            url: "save",
            data: JSON.stringify({
                save: $(this).attr("id")
            }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function(response) {
                $(clicked).load(document.URL + " #" + $(clicked).attr("id"));
            },
            error: function(error) {
                console.log("error");
            }
        });
    });
});