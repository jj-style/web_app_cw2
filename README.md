```text
 _____________________________________________________________________________________________
|   _____                                 _        _   _               ______          _ _    |
|  / ____|                               | |      | | (_)             |  ____|        | | |   |
| | (___   ___  __ _ _ __ ___   ___ _ __ | |_ __ _| |_ _  ___  _ __   | |__ __ _ _   _| | |_  |
|  \___ \ / _ \/ _` | '_ ` _ \ / _ \ '_ \| __/ _` | __| |/ _ \| '_ \  |  __/ _` | | | | | __| |
|  ____) |  __/ (_| | | | | | |  __/ | | | || (_| | |_| | (_) | | | | | | | (_| | |_| | | |_  |
| |_____/ \___|\__, |_| |_| |_|\___|_| |_|\__\__,_|\__|_|\___/|_| |_| |_|  \__,_|\__,_|_|\__| |
|               __/ |                                                                         |
|              |___/                                                                          |
|_____________________________________________________________________________________________|
```
The deployed at can be found on *pythonanywhere* at [https://jjstyle.pythonanywhere.com/](https://jjstyle.pythonanywhere.com/)  

 The requirements in ./requirements.txt are needed as some modules used which are not installed by default.  

Do this using virtual environments and pip:
- Create a virtual environment: `$python -m venv env-name`
- Activate the environment: `$source env-name/bin/activate`
- Install requirements: `(env-name)$pip install -r requirements.txt`

Run `(env-name)$python run.py` to run the app and view it locally at [127.0.0.1:5000/](127.0.0.1:5000/)